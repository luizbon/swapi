﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using DataAccess;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace DataAccess
{
    public class Repository<T> where T : IModel
    {
        public Repository(string jsonPath)
        {
            var json = File.ReadAllText(jsonPath);

            Data = JsonConvert.DeserializeObject<IList<T>>(json, new JsonSerializerSettings()
            {
                ContractResolver = new DefaultContractResolver
                {
                    NamingStrategy = new SnakeCaseNamingStrategy()
                }
            });
        }

        public IList<T> Data { get; set; }

        public T GetById(int id)
        {
            return Data.SingleOrDefault(x => x.Id == id);
        }

        public PagedList<T> GetList(int? page, int pageSize = 10)
        {
            var pageList = new PagedList<T>
            {
                Page = page ?? 1,
                TotalCount = Data.Count(),
                HasPrevious = page > 1
            };

            var startIndex = (page ?? 1 - 1) * pageSize;
            pageList.Results = Data.Skip(startIndex).Take(pageSize).ToList();
            pageList.HasNext = pageList.TotalCount - (startIndex + pageSize) > 0;

            return pageList;
        }
    }
}