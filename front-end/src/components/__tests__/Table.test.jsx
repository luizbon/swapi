import React from 'react';
import { mount } from 'enzyme';
import Table from '../Table';

const selectPerson = jest.fn();
const people = [
  {
    name: 'Luke Skywalker',
    height: '172',
    mass: '77'
  },
  {
    name: 'C-3PO',
    height: '167',
    mass: '75'
  },
  {
    name: 'R2-D2',
    height: '96',
    mass: '32'
  }
];

const setup = () =>
  mount(<Table people={people} selectPerson={selectPerson} />);

describe('Table component', () => {
  beforeEach(() => {
    selectPerson.mockClear();
  });

  test('Should match snapshot', () => {
    const wrapper = setup();
    expect(wrapper).toMatchSnapshot();
  });

  test('Should call selectPerson on row click', () => {
    const wrapper = setup();
    wrapper
      .find('tbody tr')
      .first()
      .simulate('click');
    expect(selectPerson).toBeCalledWith(people[0]);
  });
});
