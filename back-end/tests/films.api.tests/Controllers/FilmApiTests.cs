﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using DataAccess;
using films.api.Models;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using Xunit;

namespace api.tests.Controllers
{
    public class FilmApiTests: IClassFixture<ApiWebApplicationFactory<films.api.Startup>>
    {
        private readonly HttpClient _client;

        public FilmApiTests(ApiWebApplicationFactory<films.api.Startup> factory)
        {
            _client = factory.CreateClient(new WebApplicationFactoryClientOptions
            {
                AllowAutoRedirect = false
            });
        }

        [Fact]
        public async Task Get_Films_ReturnsListOfFilms()
        {
            var result = await _client.GetAsync("/api/films");

            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
            var content = JsonConvert.DeserializeObject<PagedList<Film>>(await result.Content.ReadAsStringAsync());
            Assert.True(content.TotalCount > 0);
            Assert.Equal(1, content.Page);
        }

        [Fact]
        public async Task Get_NonExistingPageFilms_Returns404()
        {
            var result = await _client.GetAsync("/api/films?page=10");

            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
        }

        [Theory]
        [InlineData(1)]
        public async Task Get_Film_ReturnsCorrectId(int id)
        {
            var result = await _client.GetAsync($"/api/films/{id}/");

            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
            var content = JsonConvert.DeserializeObject<Film>(await result.Content.ReadAsStringAsync());
            Assert.Equal(id, content.Id);
        }

        [Theory]
        [InlineData(-1)]
        public async Task Get_NonExistingFilm_Returns404(int id)
        {
            var result = await _client.GetAsync($"/api/films/{id}");

            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
        }
    }
}
