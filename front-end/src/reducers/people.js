import { RECEIVE_PEOPLE, REQUEST_PEOPLE } from '../actions/people';

export default (
  state = {
    isFetching: false
  },
  action
) => {
  switch (action.type) {
    case REQUEST_PEOPLE:
      return {
        ...state,
        isFetching: true
      };
    case RECEIVE_PEOPLE:
      const { result } = action;
      return {
        ...state,
        isFetching: false,
        next: result.next,
        previous: result.previous,
        results: result.results.map(item => ({
          name: item.name,
          height: item.height,
          mass: item.mass,
          birth_year: item.birth_year,
          gender: item.gender,
          films: item.films
        }))
      };
    default:
      return state;
  }
};
