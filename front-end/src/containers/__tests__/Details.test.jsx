import React from 'react';
import { mount } from 'enzyme';
import { Details } from '../Details';

const detailsData = {
  name: 'Luke Skywalker',
  height: '172',
  mass: '77',
  birth_year: '19BBY',
  gender: 'male',
  films: [
    'https://swapi.co/api/films/2/',
    'https://swapi.co/api/films/6/',
    'https://swapi.co/api/films/3/',
    'https://swapi.co/api/films/1/',
    'https://swapi.co/api/films/7/'
  ]
};

const setup = props =>
  mount(<Details {...props} filmTitles={props.films} dispatch={jest.fn} />);

describe('Details compoennt', () => {
  test('should match snapshot', () => {
    const wrapper = setup(detailsData);

    expect(wrapper).toMatchSnapshot();
  });

  test('onClose should be called on close button, modal toggle and header toggle', () => {
    const onClose = jest.fn();
    const wrapper = setup({ ...detailsData, onClose });

    wrapper.find('Button').simulate('click');
    wrapper.find('Modal').prop('toggle')();
    wrapper.find('ModalHeader').prop('toggle')();

    expect(onClose).toBeCalledTimes(3);
  });
});
