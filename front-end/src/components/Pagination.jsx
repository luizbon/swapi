import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Pagination as BootstrapPagination,
  PaginationItem,
  PaginationLink
} from 'reactstrap';

class Pagination extends Component {
  render() {
    const { previous, next, onClick } = this.props;
    return (
      <BootstrapPagination>
        <PaginationItem disabled={previous === null} className="uia-previous">
          <PaginationLink previous onClick={() => onClick(previous)} />
        </PaginationItem>
        <PaginationItem disabled={next === null} className="uia-next">
          <PaginationLink next onClick={() => onClick(next)} />
        </PaginationItem>
      </BootstrapPagination>
    );
  }
}

Pagination.propTypes = {
  previous: PropTypes.string,
  next: PropTypes.string,
  onClick: PropTypes.func
};

export default Pagination;
