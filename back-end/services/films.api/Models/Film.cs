﻿using System;
using System.Collections.Generic;
using DataAccess;

namespace films.api.Models
{
    public class Film: IModel
    {
        public IList<string> Starships { get; set; }

        public DateTime Edited { get; set; }

        public IList<string> Vehicles { get; set; }

        public IList<string> Planets { get; set; }

        public string Producer { get; set; }

        public string Title { get; set; }

        public DateTime Created { get; set; }

        public string Director { get; set; }

        public string ReleaseDate { get; set; }

        public string OpeningCrawl { get; set; }

        public IList<string> Characters { get; set; }

        public IList<string> Species { get; set; }

        public int Id { get; set; }
    }
}
