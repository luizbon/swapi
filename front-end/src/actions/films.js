export const REQUEST_FILMS = 'REQUEST_FILMS';
export const RECEIVE_FILMS = 'RECEIVE_FILMS';

const requestFilms = () => {
  return {
    type: REQUEST_FILMS
  };
};

const receiveFilms = result => {
  return {
    type: RECEIVE_FILMS,
    result
  };
};

const fetchFilms = films => {
  return async dispatch => {
    dispatch(requestFilms());

    const json = await Promise.all(
      films.map(film => fetch(film).then(resp => resp.json()))
    );
    return dispatch(receiveFilms(json));
  };
};

export default films => {
  return dispatch => {
    return dispatch(fetchFilms(films));
  };
};
