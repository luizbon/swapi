import people, { RECEIVE_PEOPLE, REQUEST_PEOPLE } from '../people';
import fetchMock from 'fetch-mock';

const dispatch = jest.fn(action => action);

describe('people action creator', () => {
  beforeEach(() => {
    dispatch.mockClear();
    fetchMock.restore();
  });

  test('on action call it should dispatch request and receive', async () => {
    const mockedData = { mock: 'data' };
    fetchMock.getOnce('http://swapi/', mockedData);

    await people('http://swapi/')(dispatch)(dispatch);

    expect(dispatch).toHaveBeenCalledWith({
      type: REQUEST_PEOPLE
    });
    expect(dispatch).toHaveBeenLastCalledWith({
      type: RECEIVE_PEOPLE,
      result: mockedData
    });
  });
});
