import React from 'react';
import { mount } from 'enzyme';
import Pagination from '../Pagination';

const onClick = jest.fn();
const setup = (props = {}) =>
  mount(
    <Pagination next="next" previous="previous" onClick={onClick} {...props} />
  );

describe('Pagination component', () => {
  beforeEach(() => {
    onClick.mockClear();
  });

  test('Should call OnClick when click previous', () => {
    const wrapper = setup();
    wrapper.find('.uia-previous button').simulate('click');
    expect(onClick).toHaveBeenCalledWith('previous');
  });

  test('Should call OnClick when click next', () => {
    const wrapper = setup();
    wrapper.find('.uia-next button').simulate('click');
    expect(onClick).toHaveBeenCalledWith('next');
  });

  test("Should set previous to disabled when it's null", () => {
    const wrapper = setup({ previous: null });
    expect(wrapper.find('li.uia-previous.disabled')).toBeDefined();
  });

  test("Should set next to disabled when it's null", () => {
    const wrapper = setup({
      next: null
    });
    expect(wrapper.find('li.uia-next.disabled')).toBeDefined();
  });
});
