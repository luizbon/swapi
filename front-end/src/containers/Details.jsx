import React, { Component } from 'react';
import PropTyoes from 'prop-types';
import { connect } from 'react-redux';
import { requestFilms } from '../actions';
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  ListGroup,
  ListGroupItem
} from 'reactstrap';

export class Details extends Component {
  componentDidMount() {
    const { dispatch, films } = this.props;
    dispatch(requestFilms(films));
  }

  render() {
    const {
      name,
      height,
      birth_year,
      gender,
      filmTitles,
      isFetching,
      onClose
    } = this.props;
    return (
      <Modal isOpen={true} toggle={onClose}>
        <ModalHeader toggle={onClose}>{name}</ModalHeader>
        <ModalBody>
          <dl>
            <dt>Height</dt>
            <dd>{height}</dd>
            <dt>Bitrh Year</dt>
            <dd>{birth_year}</dd>
            <dt>Gender</dt>
            <dd>{gender}</dd>
            <dt>Films</dt>
            <dd>
              {isFetching && 'Loading...'}
              {!isFetching && (
                <ListGroup>
                  {filmTitles.map(film => (
                    <ListGroupItem key={film}>{film}</ListGroupItem>
                  ))}
                </ListGroup>
              )}
            </dd>
          </dl>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={onClose}>
            Close
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

Details.propTypes = {
  name: PropTyoes.string,
  height: PropTyoes.string,
  birth_year: PropTyoes.string,
  gender: PropTyoes.string,
  filmTitles: PropTyoes.arrayOf(PropTyoes.string),
  isFetching: PropTyoes.bool,
  onClose: PropTyoes.func
};

const mapStateToProps = state => {
  return {
    filmTitles: state.films.results || [],
    isFetching: state.films.isFetching
  };
};

export default connect(mapStateToProps)(Details);
