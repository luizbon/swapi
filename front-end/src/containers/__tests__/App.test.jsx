import React from 'react';
import { mount } from 'enzyme';
import App from '../App';

test('App rendering', () => {
  const wrapper = mount(<App />);
  expect(wrapper).toMatchSnapshot();
});
