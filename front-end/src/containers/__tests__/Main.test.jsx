import React from 'react';
import { shallow } from 'enzyme';
import { Main } from '../Main';
import Details from '../Details';
import people from './people.json';

const setup = (props = { isLoading: true }) =>
  shallow(<Main dispatch={jest.fn()} {...props} />);

describe('Main container', () => {
  test('On request it should set loading state', () => {
    const wrapper = setup();
    expect(wrapper.find('table').length).toBe(0);
  });

  test('On response it should render the table', () => {
    const wrapper = setup({
      isLoading: false,
      ...people
    });
    expect(wrapper.find('Table').length).toBe(1);
    expect(wrapper).toMatchSnapshot();
  });

  test('On person select it should show details', () => {
    const wrapper = setup({
      isLoading: false,
      ...people
    });
    expect(wrapper.find(Details).length).toBe(0);
    wrapper.instance().selectPerson(people.results[0]);
    expect(wrapper.find(Details).length).toBe(1);
  });
});
