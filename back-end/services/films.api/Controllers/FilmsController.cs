﻿using DataAccess;
using films.api.Models;
using Microsoft.AspNetCore.Mvc;

namespace films.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FilmsController : ControllerBase
    {
        private readonly Repository<Film> _repository;

        public FilmsController(Repository<Film> repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public IActionResult Get(int? page)
        {
            var results = _repository.GetList(page);

            if (results.Results.Count > 0)
                return Ok(results);

            return NotFound();
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var result = _repository.GetById(id);
            if (result == null)
                return NotFound();
            return Ok(result);
        }
    }
}
