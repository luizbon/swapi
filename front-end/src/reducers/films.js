import { RECEIVE_FILMS, REQUEST_FILMS } from '../actions/films';

export default (
  state = {
    isFetching: false
  },
  action
) => {
  switch (action.type) {
    case REQUEST_FILMS:
      return {
        ...state,
        isFetching: true
      };
    case RECEIVE_FILMS:
      const { result } = action;
      return {
        ...state,
        isFetching: false,
        results: result.map(item => item.title)
      };
    default:
      return state;
  }
};
