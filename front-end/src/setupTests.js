import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import fetchMock from 'fetch-mock';
import nodeFetch from 'node-fetch';

configure({ adapter: new Adapter() });

nodeFetch.default = fetchMock.sandbox();
